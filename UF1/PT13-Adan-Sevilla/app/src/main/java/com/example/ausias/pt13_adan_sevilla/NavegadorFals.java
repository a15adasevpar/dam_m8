package com.example.ausias.pt13_adan_sevilla;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NavegadorFals extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegador_fals);
    }
    /*
    protected void isPrime(View view) {
        String txtNumber = ((EditText) findViewById(R.id.txtNumber)).getText().toString();
        int number = Integer.parseInt(txtNumber);

        if (isPrime(number))
            Toast.makeText(this, "El nombre és primer", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "El nombre no és primer", Toast.LENGTH_SHORT).show();
    }

    protected boolean isPrime(int num) {
        if (num < 2) return false;
        if (num == 2) return true;
        if (num % 2 == 0) return false;
        for (int i = 3; i * i <= num; i += 2)
            if (num % i == 0) return false;
        return true;
    }*/

    protected void isPrime(View view) {
        String txtNumber = ((EditText) findViewById(R.id.txtNumber)).getText().toString();
        new MetodeQueTrigaMolt().execute(txtNumber);
    }

    private class MetodeQueTrigaMolt extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            Long num = Long.parseLong(strings[0]);
            try{/*
                if (num < 2) return "El nombre no és primer";
                if (num == 2) return "El nombre és primer";
                if (num % 2 == 0) return "El nombre no és primer";
                for (int i = 2; i <= num; i ++)
                    if (num % i == 0) return "El nombre no és primer";
                return "El nombre és primer";
                */
                /*
                if (num < 2) return "El nombre no és primer";
                if (num == 2) return "El nombre és primer";
                if (num % 2 == 0) return "El nombre no és primer";
                for (int i = 3; i * i <=  ; i += 2)
                    if (num % i == 0) return "El nombre no és primer";
                return "El nombre és primer";
                */
                for (long i = 2; i<num; i++) {
                    if (num % i == 0)
                        return "El nombre no és primer";
                }
                return "El nombre és primer";
            }catch(Exception e) {
                return "Exception Caught";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
        }
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.interrupted();
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            TextView txt = (TextView) findViewById(R.id.txtNumber);
            txt.setText("Executed"); // txt.setText(result);
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
