package com.example.ausias.mysimplerecyclerview;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rv = findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(this));

        List<String> input = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            input.add("Lletra " + i);
        }

        ElMeuAdaptador adap = new ElMeuAdaptador(input);
        rv.setAdapter(adap);
    }
}
