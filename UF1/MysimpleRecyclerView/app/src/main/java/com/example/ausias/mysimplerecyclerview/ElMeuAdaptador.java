package com.example.ausias.mysimplerecyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ausias on 08/01/18.
 */

public class ElMeuAdaptador extends RecyclerView.Adapter<ElMeuAdaptador.ViewHolder>{

    List<String> lletres;

    ElMeuAdaptador(List<String> lletres) {
        this.lletres = lletres;
    }

    public  ElMeuAdaptador.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fila, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void onBindViewHolder(ElMeuAdaptador.ViewHolder holder, int position) {
        String dada = lletres.get(position);
        holder.tvLletra.setText(dada);
        holder.tvNum.setText(position+"");
    }

    public int getItemCount() {
        return lletres.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNum;
        public TextView tvLletra;

        public ViewHolder (View itemView){
            super(itemView);
            tvNum = itemView.findViewById(R.id.tvNumero);
            tvLletra = itemView.findViewById(R.id.tvLletra);
        }
    }
}
