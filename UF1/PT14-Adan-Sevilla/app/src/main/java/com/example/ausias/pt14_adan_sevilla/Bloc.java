package com.example.ausias.pt14_adan_sevilla;

import android.media.Image;

import java.util.Date;

/**
 * Created by ausias on 14/12/17.
 */

public class Bloc {
    private int id;
    private String horaInici;
    private float temperatura;
    private String ciudad;
    private int icona;


    public Bloc(String horaInici,float temperatura,String ciudad){
        this.horaInici = horaInici;
        this.temperatura = temperatura;
        this.ciudad = ciudad;
        this.icona = getIcono();
    }
    public int getId(){return id;}

    public String getHoraInici() {
        return horaInici;
    }

    public void setHoraInici(String horaInici) {
        this.horaInici = horaInici;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getIcono() {
        if (this.temperatura < 290) {
            return 1;
        }
        return 0;

    }

    public void setIcono(int icona) {
        this.icona = icona;
    }

}