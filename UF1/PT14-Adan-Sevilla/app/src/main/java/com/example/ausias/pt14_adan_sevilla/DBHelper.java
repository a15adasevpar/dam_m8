package com.example.ausias.pt14_adan_sevilla;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DBHelper  extends SQLiteOpenHelper {
    //version number to upgrade database version
    //each time if you Add, Edit table, you need to change the
    //version number.
    private static final int DATABASE_VERSION = 4;

    // Database Name
    private static final String DATABASE_NAME = "crud.db";

    public DBHelper(Context context ) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //All necessary tables you like to create will create here

        String CREATE_TABLE_STUDENT = "CREATE TABLE TEMPERATURAS (id INT AUTO_INCREMENT, temperatura FLOAT(3,2), ciudad VARCHAR(50), fecha DATETIME,PRIMARY KEY (id))";

        db.execSQL(CREATE_TABLE_STUDENT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertdb(float temperatura,String ciudad,String fecha){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues insertValues = new ContentValues();
        insertValues.put("temperatura", temperatura);
        insertValues.put("ciudad", ciudad);
        insertValues.put("fecha", fecha);
        db.insert("TEMPERATURAS", null, insertValues);
        db.close();
    }

    public List<Bloc> selectAll(String ciudad){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT id, temperatura, ciudad, fecha FROM TEMPERATURAS where ciudad='"+ciudad+"'";
        Cursor c = db.rawQuery(selectQuery,null);
        ArrayList<Bloc> lista = new ArrayList<Bloc>();

        while(c.moveToNext()) {
            int id =c.getInt(c.getColumnIndex("id"));
            float temperatura =c.getInt(c.getColumnIndex("temperatura"));
            String ciudad2 =c.getString(c.getColumnIndex("ciudad"));
            String fecha =c.getString(c.getColumnIndex("fecha"));
            lista.add(new Bloc(fecha,temperatura,ciudad2));
        }
        c.close();
        db.close();
        return lista;
    }

    public Bloc selectFirstBloc() {
        return new Bloc("18:00",123, "sw");
    }

    public Bloc getLastBloc(String var){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT fecha,ciudad,temperatura FROM TEMPERATURAS WHERE ciudad='"+var+"'order by id asc limit 1";
        Cursor c = db.rawQuery(selectQuery,null);
        String fecha = null, ciudad = null;
        float temperatura = 0;
        while(c.moveToFirst()) {
            fecha =c.getString(c.getColumnIndex("fecha"));
            ciudad =c.getString(c.getColumnIndex("ciudad"));
            temperatura =c.getFloat(c.getColumnIndex("temperatura"));
            c.close();
            return new Bloc(fecha,temperatura,ciudad);
        }

        c.close();
        db.close();
        return null;
    }

    public String getLastDate(String var){
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT fecha FROM TEMPERATURAS WHERE ciudad='"+var+"'order by id asc limit 1";
        Cursor c = db.rawQuery(selectQuery,null);

        while(c.moveToFirst()) {
            String fecha =c.getString(c.getColumnIndex("fecha"));
            return fecha;
        }
        c.close();
        db.close();
        return null;
    }


}

