package com.example.ausias.pt14_adan_sevilla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;

import static com.example.ausias.pt14_adan_sevilla.TemperaturesHoresActivity.ns;


public class Parsejador extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parsejador);
    }

    public ArrayList<Bloc> parse(String xml) throws XmlPullParserException, IOException {
        InputStream in =  new ByteArrayInputStream(xml.getBytes("UTF-8"));

        try {
            XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = xmlPullParserFactory.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private ArrayList<Bloc> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<Bloc> blocs = new ArrayList<>();

        // Start in the tag carteraPropiedades
        parser.require(XmlPullParser.START_TAG, ns, "weatherdata");
        Bloc bloc = null;


        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();

            // Starts by looking for the listaPropiedades tag
            if (name.equals("forecast")) {
                parser.nextTag();
                bloc = readEntry(parser);
                blocs.add(bloc);
            }else if(name.equals(("time"))){
                bloc = readEntry(parser);
                blocs.add(bloc);
            }else {
                skip(parser);
            }
        }
        return blocs;
    }

    private Bloc readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "time");
        String time = null;
        String temperatura = null;

        time = parser.getAttributeValue(ns,"from");

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }

            String name = parser.getName();
            if(name.equals("temperature")) {
                temperatura = readTemperatura(parser);
            }else{
                skip(parser);
            }
        }
        return new Bloc(time,Float.parseFloat(temperatura),PrincipalActivity.nomCiutat);
    }

    private String readTemperatura(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "temperature");
        String temperatura = null;

        temperatura = parser.getAttributeValue(null, "value");
        parser.nextTag();

        parser.require(XmlPullParser.END_TAG, ns, "temperature");
        // Log.d("Comprobacion",""+temperatura);
        return temperatura;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

}
