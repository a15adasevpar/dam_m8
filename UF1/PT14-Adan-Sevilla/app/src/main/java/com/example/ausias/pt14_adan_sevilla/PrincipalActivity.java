package com.example.ausias.pt14_adan_sevilla;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class PrincipalActivity extends AppCompatActivity {
   protected static String nomCiutat;
    Parsejador parsejador = new Parsejador();
    static final String ns = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
    }


    protected void btnClick(View view) {
        nomCiutat = ((TextView)findViewById(R.id.txtCityName)).getText().toString();
        new Descarregador(view.getContext()).execute(nomCiutat);

    }


    class Descarregador extends AsyncTask<String, Void, String> {
        Context context;
        public Descarregador(Context context) {
            this.context = context;
        }

        protected String doInBackground(String... nomCiutats) {
            URL url;
            HttpURLConnection urlConnection = null;
            String theString = "";
            try {
                DBHelper dbhelper = new DBHelper(this.context);
                String a = dbhelper.getLastDate(nomCiutats[0]);
                if (a == a) {
                    Log.d("Message", "No hi ha res a la bdd, consultem tot el xml");
                    url = new URL("http://api.openweathermap.org/data/2.5/forecast?q=" + nomCiutats[0] + "&mode=xml&appid=7fb2cfe2c9183173719485bb2a0a0e1a");
                    Log.d("Fent connexió", "value: " + url.toString());
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    Scanner s = new Scanner(in).useDelimiter("\\A");
                    theString = s.hasNext() ? s.next() : "";
                    urlConnection.disconnect();
                }

                else {
                    Date dataActual = new java.util.Date();
                    Log.d("Hora actual", dataActual.getDate()+"/" + dataActual.getMonth() + "/" + dataActual.getYear());
                    Log.d("Hora actual", dataActual.getHours()+":" + dataActual.getMinutes() + ":" + dataActual.getSeconds());
                    Log.d("Última hora: ", a);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                Log.d("My string", "value: " + theString);
            }
            return theString;
        }

        protected void onPostExecute(String xmlInfo) {
            //Toast.makeText(getApplicationContext(), "he acabat l'async task de " + xmlInfo, Toast.LENGTH_SHORT).show();
            try {

                Log.d("COnsulta string",""+xmlInfo);
                ArrayList<Bloc> arr_list =  parsejador.parse(xmlInfo);
                DBHelper dbHelper = new DBHelper(context);
                for (Bloc bloc: arr_list) {
                    Log.d("Guardar BDD",bloc.getHoraInici());
                    dbHelper.insertdb(bloc.getTemperatura(),bloc.getCiudad(),bloc.getHoraInici());
                }
                dbHelper.close();
                Intent segona_activitat = new Intent(getApplicationContext(), TemperaturesHoresActivity.class);
                startActivity(segona_activitat);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

