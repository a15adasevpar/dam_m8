package com.example.ausias.pt14_adan_sevilla;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ausias on 08/01/18.
 */

public class ElMeuAdaptador extends RecyclerView.Adapter<ElMeuAdaptador.ViewHolder>{

    List<Bloc> blocs;

    ElMeuAdaptador(List<Bloc> blocs) {
        this.blocs = blocs;
    }

    public  ElMeuAdaptador.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fila, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public void onBindViewHolder(ElMeuAdaptador.ViewHolder holder, int position) {
        Bloc bloc = blocs.get(position);
        holder.tvFecha.setText(bloc.getHoraInici());
        String parseTemperatura = new String(bloc.getTemperatura()+"");
        holder.tvTemperatura.setText(parseTemperatura);
        holder.tvIcono.setText(bloc.getIcono()+"");
    }

    public int getItemCount() {
        return blocs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFecha;
        public TextView tvTemperatura;
        public TextView tvIcono;

        public ViewHolder (View itemView){
            super(itemView);
            tvIcono = itemView.findViewById(R.id.tvIcono);
            tvTemperatura = itemView.findViewById(R.id.tvTemperatura);
            tvFecha = itemView.findViewById(R.id.tvFecha);
        }
    }
}
