package com.example.ausias.pt14_adan_sevilla;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TemperaturesHoresActivity extends AppCompatActivity {

    static final String ns = null;
    Context context;
    List<Bloc> blocs;
    RecyclerView rv;

    public TemperaturesHoresActivity() {
        this.blocs = new ArrayList<Bloc>();
    }

    public TemperaturesHoresActivity(Context context) {
        this.context = getApplicationContext();
        this.blocs = new ArrayList<Bloc>();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_temperatures_hores);
        rv = findViewById(R.id.recyclerView);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        new Consultor(context).execute(new String(PrincipalActivity.nomCiutat));
    }

    class Consultor extends AsyncTask<String, Void, List<Bloc>> {
        Context context;

        public Consultor(Context context) { this.context = getApplicationContext();}

        @Override
        protected List<Bloc> doInBackground(String... strings) {
            Log.d("message", strings[0]);
                DBHelper dbhelper = new DBHelper(context);
                blocs = dbhelper.selectAll(PrincipalActivity.nomCiutat);
                Log.d("message","mirem si el bloc es null");
                if (blocs == null)
                    return null;
                if (blocs.isEmpty())
                    return null;
                return blocs;
        }

        @Override
        protected void onPostExecute(List<Bloc> blocs) {
            ElMeuAdaptador adap = new ElMeuAdaptador(blocs);
            rv.setAdapter(adap);
            super.onPostExecute(blocs);
        }
    }



}
