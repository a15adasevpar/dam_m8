package com.example.ausias.pt21_adan_sevilla;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private SoundPool soundPool;
    private int soundID1, soundID2, soundID3, soundID4;
    boolean loaded = false;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                loaded = true;
            }
        });
        soundID1 = soundPool.load(this, R.raw.sound1, 1);
        soundID2 = soundPool.load(this, R.raw.sound2, 1);
        soundID3 = soundPool.load(this, R.raw.sound3, 1);
        soundID4 = soundPool.load(this, R.raw.sound4, 1);

        mediaPlayer = new MediaPlayer();
        String url = "https://vignette.wikia.nocookie.net/los-inmortales/images/c/c4/Mos_Eisley_Cantina_Theme_HQ_.ogg/revision/latest?cb=20160301032751&path-prefix=es";


        try {
            mediaPlayer.setDataSource(url);
        }catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(MainActivity.this, "PREPARANT!!!!!", Toast.LENGTH_SHORT).show();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                loaded = true;
                Toast.makeText(MainActivity.this, "LOADED!!!!!", Toast.LENGTH_SHORT).show();
            }
        });
        mediaPlayer.prepareAsync();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.item1) {
            if (loaded) {
                mediaPlayer.start();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void clickBtn1 (View view) {
        if (loaded) {
            soundPool.play(soundID1, 1, 1, 1, 0, 1.0f);
            Log.e("Test", "Played sound 1");
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
        }
    }
    public void clickBtn2 (View view) {
        if (loaded) {
            soundPool.play(soundID2, 1, 1, 1, 0, 1.0f);
            Log.e("Test", "Played sound 2");
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
        }
    }
    public void clickBtn3 (View view) {
        if (loaded) {
            soundPool.play(soundID3, 1, 1, 1, 0, 1.0f);
            Log.e("Test", "Played sound 3");
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
        }
    }
    public void clickBtn4 (View view) {
        if (loaded) {
            soundPool.play(soundID4, 1, 1, 1, 0, 1.0f);
            Log.e("Test", "Played sound 4");
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
        }
    }
}
