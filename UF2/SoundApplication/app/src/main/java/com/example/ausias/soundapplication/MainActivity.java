package com.example.ausias.soundapplication;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private SoundPool soundPool;
    private int soundID;
    boolean loaded = false;

    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                //loaded = true;
            }
        });
        soundID = soundPool.load(this, R.raw.explosion, 1);

        //Second example

        mediaPlayer = new MediaPlayer();
        //String url = "http://archive.org/download/Free_20s_Jazz_Collection/Vo_Do_Do.ogg";
        //String url = "https://vignette.wikia.nocookie.net/los-inmortales/images/c/c4/Mos_Eisley_Cantina_Theme_HQ_.ogg/revision/latest?cb=20160301032751&path-prefix=es";
        String url = "https://vignette.wikia.nocookie.net/starwars/images/a/ac/Force_Theme.ogg/revision/latest?cb=20100929162831";


        try {
            mediaPlayer.setDataSource(url);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void clickBtn (View view) {
        if (loaded) {
            soundPool.play(soundID, 1, 1, 1, 0, 1.0f);
            Log.e("Test", "Played sound");
        }
    }

    public void onPlay(View view) {
        if (loaded) {
            mediaPlayer.start();
        }
    }

    public void onPrepare(View view) {
        Toast.makeText(MainActivity.this, "PREPARANT!!!!!", Toast.LENGTH_SHORT).show();
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                loaded = true;
                Toast.makeText(MainActivity.this, "LOADED!!!!!", Toast.LENGTH_SHORT).show();
            }
        });
        mediaPlayer.prepareAsync();
    }
}
